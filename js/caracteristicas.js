function themeSwitcher(element) {
    let src = "";
    switch (element.id) {
        case "white":
            src = "imagenes/img_feature_5_white.png"
            break;
        case "dark":
            src = "imagenes/img_feature_5_dark.png"
            break;
        case "black":
            src = "imagenes/img_feature_5_black.png"
            break;
        default:
            break;
    }
    document.getElementById("theme_img_placeholder").src = src;
}

function colorSwitcher(element) {
    let rgbColor = "";
    switch (element.id) {
        case "blue_a400":
            rgbColor = "#2979ff"
            break;
        case "red_a400":
            rgbColor = "#ff1744"
            break;
        case "green_a400":
            rgbColor = "#00e676"
            break;
        case "orange_a400":
            rgbColor = "#ff9100"
            break;
        case "pink_a400":
            rgbColor = "#ff80ab"
            break;
    }
    document.getElementById("theme_img_placeholder").style.backgroundColor = rgbColor
}

let switcherCount = 0
let imgs = ["img_feature_6_card", "img_feature_6_flat", "img_feature_6_abbey", "img_feature_6_material", "img_feature_6_blur"]

function nowPlayingScreenSwitcher() {
    switcherCount++;
    if (switcherCount == 5) {
        switcherCount = 0
    }

    document.getElementById("now_playing_screen_img").src = "imagenes/" + imgs[switcherCount] + ".png";
}